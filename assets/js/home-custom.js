// ===========================lightbox js============================



var mySwiper = new Swiper(".swiper-container-lightbox", {

  loop: true,
  slidesPerView: "5",
  spaceBetween: 10,
  centeredSlides: true,
  slideToClickedSlide: false,
  // autoplay: {
  //   delay: 3000,
  //   disableOnInteraction: true 
  // },
  // If we need pagination
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
    renderBullet: function (index, className) {
      return '<span class="' + className + '">' + (index + 1) + "</span>";
    }
  },
  // Navigation arrows
  // navigation: {
  //   nextEl: '.swiper-button-next',
  //   prevEl: '.swiper-button-prev',
  // },
  // keyboard control
  keyboard: {
    enabled: true,
  }
});


var initPhotoSwipeFromDOM = function (gallerySelector) {

  var parseThumbnailElements = function (el) {
    var thumbElements = el.childNodes,
      numNodes = thumbElements.length,
      items = [],
      figureEl,
      linkEl,
      size,
      item;

    for (var i = 0; i < numNodes; i++) {
      figureEl = thumbElements[i]; // <figure> element


      if (figureEl.nodeType !== 1) {
        continue;
      }

      linkEl = figureEl.children[0];

      size = linkEl.getAttribute("data-size").split("x");


      item = {
        src: linkEl.getAttribute("href"),
        w: parseInt(size[0], 10),
        h: parseInt(size[1], 10)
      };

      if (figureEl.children.length > 1) {

        item.title = figureEl.children[1].innerHTML;
      }

      if (linkEl.children.length > 0) {

        item.msrc = linkEl.children[0].getAttribute("src");
      }

      item.el = figureEl;
      items.push(item);
    }

    return items;
  };


  var closest = function closest(el, fn) {
    return el && (fn(el) ? el : closest(el.parentNode, fn));
  };


  var onThumbnailsClick = function (e) {
    e = e || window.event;
    e.preventDefault ? e.preventDefault() : (e.returnValue = false);

    var eTarget = e.target || e.srcElement;


    var clickedListItem = closest(eTarget, function (el) {
      return el.tagName && el.tagName.toUpperCase() === "LI";
    });

    if (!clickedListItem) {
      return;
    }


    var clickedGallery = clickedListItem.parentNode,
      childNodes = clickedListItem.parentNode.childNodes,
      numChildNodes = childNodes.length,
      nodeIndex = 0,
      index;

    for (var i = 0; i < numChildNodes; i++) {
      if (childNodes[i].nodeType !== 1) {
        continue;
      }

      if (childNodes[i] === clickedListItem) {
        index = nodeIndex;
        break;
      }
      nodeIndex++;
    }

    if (index >= 0) {

      openPhotoSwipe(index, clickedGallery);
    }
    return false;
  };


  var photoswipeParseHash = function () {
    var hash = window.location.hash.substring(1),
      params = {};

    if (hash.length < 5) {
      return params;
    }

    var vars = hash.split("&");
    for (var i = 0; i < vars.length; i++) {
      if (!vars[i]) {
        continue;
      }
      var pair = vars[i].split("=");
      if (pair.length < 2) {
        continue;
      }
      params[pair[0]] = pair[1];
    }

    if (params.gid) {
      params.gid = parseInt(params.gid, 10);
    }

    return params;
  };

  var openPhotoSwipe = function (
    index,
    galleryElement,
    disableAnimation,
    fromURL
  ) {
    var pswpElement = document.querySelectorAll(".pswp")[0],
      gallery,
      options,
      items;

    items = parseThumbnailElements(galleryElement);

    // #################### 3/4 define photoswipe options (if needed) #################### 
    // https://photoswipe.com/documentation/options.html //
    options = {
      /* "showHideOpacity" uncomment this If dimensions of your small thumbnail don't match dimensions of large image */
      //showHideOpacity:true,

      // Buttons/elements
      closeEl: true,
      captionEl: true,
      fullscreenEl: true,
      zoomEl: true,
      shareEl: false,
      counterEl: false,
      arrowEl: true,
      preloaderEl: true,
      // define gallery index (for URL)
      galleryUID: galleryElement.getAttribute("data-pswp-uid"),
      getThumbBoundsFn: function (index) {
        // See Options -> getThumbBoundsFn section of documentation for more info
        var thumbnail = items[index].el.getElementsByTagName("img")[0], // find thumbnail
          pageYScroll =
          window.pageYOffset || document.documentElement.scrollTop,
          rect = thumbnail.getBoundingClientRect();

        return {
          x: rect.left,
          y: rect.top + pageYScroll,
          w: rect.width
        };
      }
    };

    // PhotoSwipe opened from URL
    if (fromURL) {
      if (options.galleryPIDs) {
        // parse real index when custom PIDs are used
        // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
        for (var j = 0; j < items.length; j++) {
          if (items[j].pid == index) {
            options.index = j;
            break;
          }
        }
      } else {
        // in URL indexes start from 1
        options.index = parseInt(index, 10) - 1;
      }
    } else {
      options.index = parseInt(index, 10);
    }

    // exit if index not found
    if (isNaN(options.index)) {
      return;
    }

    if (disableAnimation) {
      options.showAnimationDuration = 0;
    }

    // Pass data to PhotoSwipe and initialize it
    gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();

    /* ########### PART 4 - EXTRA CODE  ########### */
    /* EXTRA CODE (NOT FROM photoswipe CORE) - 
    1/2. UPDATE SWIPER POSITION TO THE CURRENT ZOOM_IN IMAGE (BETTER UI) */
    // photoswipe event: Gallery unbinds events
    // (triggers before closing animation)
    gallery.listen("unbindEvents", function () {
      // This is index of current photoswipe slide
      var getCurrentIndex = gallery.getCurrentIndex();
      // Update position of the slider
      mySwiper.slideTo(getCurrentIndex, false);
      // 2/2. Start swiper autoplay (on close - if swiper autoplay is true)
      mySwiper.autoplay.start();
    });
    // 2/2. Extra Code (Not from photo) - swiper autoplay stop when image zoom */
    gallery.listen('initialZoomIn', function () {
      if (mySwiper.autoplay.running) {
        mySwiper.autoplay.stop();
      }
    });
  };

  // loop through all gallery elements and bind events
  var galleryElements = document.querySelectorAll(gallerySelector);

  for (var i = 0, l = galleryElements.length; i < l; i++) {
    galleryElements[i].setAttribute("data-pswp-uid", i + 1);
    galleryElements[i].onclick = onThumbnailsClick;
  }

  // Parse URL and open gallery if it contains #&pid=3&gid=1
  var hashData = photoswipeParseHash();
  if (hashData.pid && hashData.gid) {
    openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
  }
};

// execute above function
initPhotoSwipeFromDOM(".my-gallery");
//======================= lightbox js ends ============================
$(document).ready(function () {
    var wid = $(window).width();
    if (wid < 768) {
      // $(document).ready(function () {
      //   $('.sidebar-list li').find('a').attr('href', 'javascripti:void(0);');
      // });
      $('.sidebar-list').find('li').first().find('a').attr('href', '#')
      // $('.sidebar-list li:nth-of-type(2)').click(function () {
      //   window.location = "/events";
      // });
      // $('.sidebar-list li:nth-of-type(3)').click(function () {
      //   window.location = "https://admissions.mastersunion.org";
      // });
      // $(document).ready(function () {
      //   $('.sidebar-list li:nth-of-type(4)').click(function () {
      //     window.location.href = 'tel:18001205288'
      //   });
      // });
      $(document).ready(function () {
        $('.sidebar-list li:nth-of-type(5)').attr("onclick", "$crisp.push(['do', 'chat:open']);$crisp.push(['do', 'chat:show']);");
        $('.sidebar-list li:nth-of-type(1)').click(function () {
          $('.DownloadBrochureForm').toggleClass('active');
        });
      });
    }
  })


  !(function (e) {
    "use strict";

    function s(s) {
      var a = e(document).scrollTop();
      e("a.smoothclick").each(function () {
        var s = e(this),
          t = e(s.attr("href"));
        console.log(t)
        t.position().top - 200 <= a && t.position().top + t.height() > a ?
          s.addClass("active") :
          s.removeClass("active");
      });
    }

    // $('.main-section').addClass('topBarActive');

    // $(".event-topBar").addClass("hide");
    e(document).ready(function () {
        if ($(".event-topBar")[0]) {
          $(".event-topBar span").on("click", function () {
            $(this).parents(".event-topBar").addClass("hide");
            $(".main-section").removeClass("topBarActive");
            sessionStorage.setItem("isTopBar", true);
          });
          if (sessionStorage.getItem("isTopBar")) {
            $(".event-topBar").addClass("hide");
            $(".main-section").removeClass("topBarActive");
          } else {
            $(".main-section").addClass("topBarActive");
          }
        }

        function a(s) {
          10 < e(s).scrollTop() ?
            e(".main-section").addClass("sticky") :
            e(".main-section").removeClass("sticky"),
            11 < e(s).scrollTop() ?
            e(".homeheader").removeClass("lighttheme ") :
            e(".homeheader").addClass("lighttheme ");
        }
        e(document).on("scroll", s),
          e(".secondary-menu-nav li a").on("click", function () {
            e(".secondary-menu-nav li a").removeClass("active"),
              e(this).addClass("active");
          }),
          e("a.tab-close").on("click", function () {
            e("a.tab-close").parent().removeClass("active");
          }),
          e(".academics .sec-box").hover(function () {
            e(".academics .sec-box").removeClass("active"),
              e(this).addClass("active");
          }),
          e(".tab1-list-items").hover(function () {
            e(".tab1-list-items").removeClass("active"), e(this).addClass("active");
          }),
          e(".scholarship-outr ul li").hover(function () {
            e(".scholarship-outr ul li").removeClass("active"),
              e(this).addClass("active");
          }),
          e(".banner-items").hover(function () {
            e(".banner-items").removeClass("active"), e(this).addClass("active");
          }),
          e("a.smoothclick,.smoothclick").on("click", function (a) {
            if ((e(document).off("scroll", s), "" !== this.hash)) {
              a.preventDefault();
              var t = this.hash;
              e("html, body").animate({
                    scrollTop: e(t).offset().top - 150,
                  },
                  1e3,
                  function () {
                    e(document).on("scroll", s);
                  }
                ),
                (window.location.hash = this.hash);
            }
          }),
          e(window).width() < 821 ?
          e("header.header-main.homeheader")
          .parent()
          .addClass("homeheader_outr") :
          e("header.header-main.homeheader")
          .parent()
          .removeClass("homeheader_outr"),
          e(window).scroll(function () {
            a(this);
          }),
          a(this),
          new Swiper(".swiper-container", {
            pagination: ".swiper-pagination",
            effect: "coverflow",
            grabCursor: !0,
            centeredSlides: !0,
            slidesPerView: "auto",
            nextButton: ".swiper-button-next",
            prevButton: ".swiper-button-prev",
            loop: !0,
            coverflow: {
              rotate: 0,
              stretch: 312,
              depth: 350,
              modifier: 1,
              slideShadows: !0,
            },
          }),
          new Swiper(".urban-map-slider", {
            pagination: ".swiper-pagination",
            effect: "coverflow",
            grabCursor: !1,
            centeredSlides: !0,
            nextButton: ".swiper-button-next",
            prevButton: ".swiper-button-prev",
            slidesPerView: "auto",
            loop: !0,
            coverflow: {
              rotate: 0,
              stretch: -10,
              depth: 10,
              modifier: 1,
              slideShadows: !0,
            },
          }),
          e(".menu-program").on("click", function (s) {
            e(".secondary-menu-nav").toggleClass("mobile-menu");
          }),
          e(".show-content").on("click", function (s) {
            e(".researchlast-boxinner").toggleClass("content-full");
          }),
          e(".hamburger").on("click", function (s) {
            e(".navigation-outer").addClass("mobile-menu");
          }),
          e(".hamburger-close").on("click", function (s) {
            e(".navigation-outer").removeClass("mobile-menu");
          }),
          e(".checklisttext-overflow").on("click", function (s) {
            e(this).parent().toggleClass("textoverflow-slide");
          }),
          e("nav ul:first").show("fast"),
          e("nav li").on("click", function (s) {
            var a = e(s.target).next("ul");
            return (
              0 == a.length ||
              (a.is(":visible") ? a.slideUp("fast") : a.slideDown("fast"), !1)
            );
          }),
          e(".mobile-menu .ti-menu").on("click", function () {
            e("nav").css("left", "0"),
              setTimeout(function () {
                e(".mobile-menu .ti-close").css("display", "block");
              }, 300);
          }),
          e(".mobile-menu .ti-close").on("click", function () {
            e("nav").css("left", "-100%"),
              e(".mobile-menu .ti-close").css("display", "none");
          }),
          e(".header-2 nav img").on("click", function () {
            e("nav span").fadeToggle();
          }),
          e(".search .ti-close").on("click", function () {
            e(".search").fadeOut();
          }),
          e(".search input").on("keypress", function (s) {
            13 == s.which && (e(".search").fadeOut(), e(".search input").val(""));
          }),
          e(".social-icon .ti-search").on("click", function () {
            e(".search").fadeIn();
          }),
          e("#toTop").on("click", function () {
            return (
              e("html, body").animate({
                  scrollTop: 0,
                },
                1e3
              ), !1
            );
          }),
          e(window).scroll(function () {
            400 < e(this).scrollTop() ?
              e("#toTop").fadeIn() :
              e("#toTop").fadeOut();
          }),
          e(".slider").owlCarousel({
            loop: !0,
            nav: !1,
            dots: !1,
            autoWidth: !0,
            autoplay: !0,
            smartSpeed: 1e3,
            responsive: {
              0: {
                items: 1,
              },
              600: {
                items: 1,
              },
              1000: {
                items: 1,
              },
            },
          }),
          e("a[href='#']").on("click", function (e) {
            e.preventDefault();
          }),
          e(".gallery-carousel").owlCarousel({
            loop: !0,
            nav: !1,
            dots: !1,
            autoplay: !0,
            smartSpeed: 1e3,
            responsive: {
              0: {
                items: 1,
              },
              600: {
                items: 2,
              },
              1000: {
                items: 3,
              },
              1300: {
                items: 4,
              },
            },
          }),
          //  e(".our-value-carousel").owlCarousel({
          //     loop: !0,
          //     nav: !0,
          //     dots: !1,
          //     margin: 30,
          //     autoplay: !1,
          //     smartSpeed: 1e3,
          //     responsive: {
          //         0: {
          //             items: 1
          //         },
          //         600: {
          //             items: 2
          //         },
          //         1000: {
          //             items: 3
          //         }
          //     }
          // }),
          // e(".news-carousel").owlCarousel({
          //     loop: !0,
          //     nav: !0,
          //     navText: ['<i class="ti-angle-left"></i>', '<i class="ti-angle-right"></i>'],
          //     dots: !1,
          //     margin: 30,
          //     autoplay: !0,
          //     smartSpeed: 1e3,
          //     responsive: {
          //         0: {
          //             items: 1
          //         },
          //         600: {
          //             items: 2
          //         },
          //         1000: {
          //             items: 3
          //         }
          //     }
          // }),
          // e(".our-value-carousel-2").owlCarousel({
          //     loop: !0,
          //     nav: !1,
          //     dots: !1,
          //     margin: 30,
          //     autoplay: !0,
          //     smartSpeed: 1e3,
          //     responsive: {
          //         0: {
          //             items: 1
          //         },
          //         600: {
          //             items: 1
          //         },
          //         1000: {
          //             items: 1
          //         }
          //     }
          // }),
          e(".set > a").on("click", function () {
            e(this).hasClass("active") ?
              (e(this).removeClass("active"),
                e(this).siblings(".content").slideUp(200),
                e(".set > a i")
                .removeClass("ti-angle-down")
                .addClass("ti-angle-right")) :
              (e(".set > a i")
                .removeClass("ti-angle-down")
                .addClass("ti-angle-right"),
                e(this)
                .find("i")
                .removeClass("ti-angle-right")
                .addClass("ti-angle-down"),
                e(".set > a").removeClass("active"),
                e(this).addClass("active"),
                e(".content").slideUp(200),
                e(this).siblings(".content").slideDown(200));
          });
      }),
      e(window).on("load", function () {
        e("#preloader").fadeOut(500);
      });
  })(jQuery),
  $(".corporate-list.owl-carousel-custom").owlCarousel({
    loop: !1,
    margin: 30,
    nav: !0,
    navText: [
      "<span class='show-for-sr prev'><i class='material-icons'>arrow_right_alt</i>back</span>",
      "<span class='show-for-sr next'>Next<i class='material-icons'>arrow_right_alt</i></span>",
    ],
    stagePadding: 10,
    responsive: {
      0: {
        items: 1,
        margin: 0,
        stagePadding: 0,
      },
      600: {
        items: 2,
        margin: 0,
        stagePadding: 0,
      },
      821: {
        items: 2,
        margin: 20,
        stagePadding: 0,
      },
      900: {
        items: 3,
      },
    },
  }),
  $(".corporate-list.shadow-view").owlCarousel({
    loop: !1,
    margin: 10,
    nav: !0,
    navText: [
      "<span class='show-for-sr prev'><i class='material-icons'>arrow_right_alt</i>back</span>",
      "<span class='show-for-sr next'>Next<i class='material-icons'>arrow_right_alt</i></span>",
    ],
    responsive: {
      0: {
        items: 1,
        stagePadding: 0,
      },
      600: {
        items: 2,
        margin: 0,
        stagePadding: 0,
      },
      821: {
        items: 2,
        margin: 20,
        stagePadding: 0,
      },
      900: {
        items: 3,
      },
    },
  }),
  $(".owl-carousel-custom").owlCarousel({
    loop: !1,
    margin: 30,
    nav: !0,
    navText: [
      "<span class='show-for-sr prev'><i class='material-icons'>arrow_right_alt</i>back</span>",
      "<span class='show-for-sr next'>Next<i class='material-icons'>arrow_right_alt</i></span>",
    ],
    stagePadding: 10,
    responsive: {
      0: {
        items: 1,
        margin: 20,
        stagePadding: 10,
      },
      600: {
        items: 2,
        margin: 20,
        stagePadding: 10,
      },
      1000: {
        items: 3,
      },
    },
  }),


  //  $(".highly-list-carousel").owlCarousel({
  //     loop: !1,
  //     margin: 0,
  //     autoWidth: !0,
  //     nav: !0,
  //     navText: ["<span class='show-for-sr prev'><i class='material-icons'>arrow_right_alt</i> </span>", "<span class='show-for-sr next'> <i class='material-icons'>arrow_right_alt</i></span>"],
  //     stagePadding: 0,
  //     responsive: {
  //         0: {
  //             items: 1
  //         },
  //         601: {
  //             items: 2
  //         },
  //         901: {
  //             items: 3
  //         },
  //         1200: {
  //             items: 4
  //         }
  //     }
  // }),
  $(document).ready(function () {
    $('input[type="range"]').on("change mousemove", function () {
      var e =
        ($(this).val() - $(this).attr("min")) /
        ($(this).attr("max") - $(this).attr("min"));
      $(this).css(
        "background-image",
        "-webkit-gradient(linear, left top, right top, color-stop(" +
        e +
        ", #2f466b), color-stop(" +
        e +
        ", #d3d3db))"
      );
    });
  });

$("a.floatDownloadBrochure").on("click", function (){
  $(".DownloadBrochureForm").toggleClass("active");
});

$(".closeBrochure").on("click", function () {
  $(this).parents(".DownloadBrochureForm").removeClass("active");
});
$(".close-enquire-Form").on("click", function () {
  $(this).parents(".float-enquire-form").removeClass("active");
});

$(".sidebar-outer .sidebar-list li:nth-child(1) a").on("click", function () {
  if( $(".float-enquire-form").hasClass("active") ){
    $(".float-enquire-form").removeClass("active");
  
  }
  
});
$(".sidebar-outer .sidebar-list li:nth-child(2) a").on("click", function () {
  if( $(".DownloadBrochureForm").hasClass("active") ){
    $(".DownloadBrochureForm").removeClass("active");
    
  }
  
});



// var wid = $( window ).width();;
//     $(wid > 821).on('load', function(){
//         alert("f");
//     });

// var wid = $( window ).width();
//   if(wid < 821) {
//     setTimeout(function(){
//         $('a.floatDownloadBrochure').addClass('hide');
//         $('.DownloadBrochureOuter').addClass('hide');
//     }, 5000);
//     }
//     else {

//     }