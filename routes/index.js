let express = require("express");
let router = express.Router();
let path = require("path");
const Handlebars = require("handlebars");
const axios = require("axios");

// var resumeDetails = {};
// Handlebars.registerHelper("prod", () => {
//   console.log("came in here....header");
//   return process.env.NODE_ENV === "prod";
// });

// Handlebars.registerHelper("apiBaseURL", () => {
//   // if (process.env.NODE_ENV === "prod")
//   //   return process.env.PROD_API_MASTERUNION_ORG;
//   return process.env.DEV_API_INVESTMENT_FUND;
// });

// Get Homepage
router.get("/", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("main", {
    title: "CPR",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});


// Get BNPL jessica  and shobit blog
router.get("/blog-bnpl-1", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("blog", {
    title: "CPR-blog",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});

// Get ClubHouse, Radio & Virality blog
router.get("/blog-clubhouse-2", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("blog-2", {
    title: "BLog-ClubHouse, Radio & Virality",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});

// Get ClubHouse, Radio & Virality blog
router.get("/blog-multiSided-3", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("blog-3", {
    title: "BLog-ClubHouse, Radio & Virality",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
});

router.get("/previous-talks", function (req, res, next) {
  //   res.render("index", { layout: "main", template: "index" });
  res.render("previous-talks", {
    title: "CPR Video",
    // isProd: process.env.NODE_ENV === "prod",
    description: "",
    metaData: {
      img: "",
      url: "",
    },
  });
  
});

module.exports = router;